// 1. What directive is used by Node.js in loading the modules it needs?
// IMPORT DIRECTIVE


// 2. What Node.js module contains a method for server creation?
// HTTP MODULE


// 3. What is the method of the http object responsible for creating a server using Node.js?
// createServer() method


// 4. What method of the response object allows us to set status codes and content types?
// Content-Type


// 5. Where will console.log() output its contents when run in Node.js?
// Terminal


// 6. What property of the request object contains the address's endpoint?
// url

